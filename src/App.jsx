import 'bootstrap/dist/css/bootstrap.min.css'
import Component1 from './Component1.jsx';
import Component2 from './Component2.jsx';
import AddComponent from './AddComponent.jsx';
import Todoitems from './Todoitems'
import Greeting from './Greeting'
import  TodoItemContextProvider  from './store/todo-items-store.jsx';
import {useReducer} from "react";



function App() {
return (
    <>

<TodoItemContextProvider>

<Component1></Component1>
<Component2></Component2>
<Todoitems></Todoitems>
<Greeting></Greeting>
</TodoItemContextProvider>

    </>
  )
}

export default App;
