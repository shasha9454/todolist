import styles from './Greeting.module.css';
import {useContext} from "react";
import {TodoitemsContext} from './store/todo-items-store.jsx';
function Greeting(){
const {todoItems} = useContext(TodoitemsContext);

return (
  <center className={styles.greeting}>
{todoItems.length == 0 && <p>Enjoy Your Day!</p>}
</center>


) ;
}
export default Greeting;
