import { createContext } from "react";
import {useReducer} from "react";


export const TodoitemsContext = createContext({
todoItems:[],
addNewItems: ()=>{},
deleteItems: ()  =>{},

});
// Context ApI Method ....
const TodoitemReducer = (CurrTodoItem , action) =>{
  let newTodoitem = CurrTodoItem;
if(action.type === "New_Item"){
  newTodoitem = [
 ...CurrTodoItem,{
  name: action.payload.todoName,
   dueDate:action.payload.setduedate,
 },

];

}else if(action.type === "Delete_Item"){
newTodoitem = CurrTodoItem.filter((item) => item.name !== action.payload.todoName );
console.log("Delted items are : " + newTodoitem);

  };

  return newTodoitem;
}
const TodoItemContextProvider = ({children}) =>{


  /*
  const InitialtodoItems=[
  {
    name:"Buy Item",
    dueDate:"8/10/2024",

  },
  ];
  const[todoItems , setTodoitems] = useState(InitialtodoItems);
  */
  const [todoItems,dispatchReducer] = useReducer(TodoitemReducer,[]);

  const addNewItems = (todoName , setduedate ) => {
    const newItemaction = {
    type: "New_Item",
    payload:{
      todoName,
      setduedate,
    },
  };
  dispatchReducer(newItemaction);
  }

  const deleteItems = (items) => {

  const deleteItemsName ={
    type: "Delete_Item",
    payload:{
      todoName:items,
    },
  };
  dispatchReducer(deleteItemsName);

  };

  return (
    <TodoitemsContext.Provider value={{
    todoItems:todoItems,
    addNewItems:addNewItems,
    deleteItems:deleteItems,
  }}>
  {children}
  </TodoitemsContext.Provider >

);
}
export default TodoItemContextProvider;
