
import styles from './Component2.module.css';
import {useState,useRef} from 'react';
import {BiMessageAdd} from 'react-icons/bi'
import {useContext} from "react";
import {TodoitemsContext} from './store//todo-items-store.jsx';
function Component2() {
  const {addNewItems} = useContext(TodoitemsContext);

const [currItem , setItem] = useState("");
const [currdueDate , setduedate] = useState("");

const handleCurrItems =(event) =>{
  
setItem(event.target.value);
}

const handledueDate = (event) =>{
setduedate(event.target.value);
}
const  handlefunctonality =()=> {
/*  event.preventDefault(); */
  addNewItems(currItem , currdueDate);
  setItem("");
  setduedate("");
}
/*
const AddItem = useRef();
const AdddueDate = useRef();
const handlecurrentItems = (event) => {
setCurrItem(event.target.value);
}
const handlesetCurrduedate = (event) => {
  setCurrduedate(event.target.value);
}
*/
//Retain mutable values without re-render ;
/*
const handlefunctonality = (event) => {
event.preventDefault();
const fAddItem = AddItem.current.value ;
const fAddDueDate= AdddueDate.current.value;
AddItem(fAddItem , fAddDueDate);
AddItem.current.value = "";
AdddueDate.current.value =""; */



return (
<>
<center>
<div className="container text-center">
  <form className="row">

    <div className="col kg-place">
    <input type="text" placeholder="Enter todo here " value ={currItem} onChange ={()=>handleCurrItems(event)} />
    </div>
    <div className="col kg-date">
      <input type="Date"  value ={currdueDate} onChange ={() => handledueDate(event)}/>
    </div>
    <div className="col">
    <button type="button" className="btn btn-success" onClick ={handlefunctonality} ><BiMessageAdd/></button>

    </div>
  </form>
</div>
</center>
</>
);


}
export default Component2;
