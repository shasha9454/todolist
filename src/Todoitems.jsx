import AddComponent from './AddComponent';
import {TodoitemsContext} from './store/todo-items-store.jsx';
import {useContext} from "react";

const Todoitems = () =>{

const createObj = useContext(TodoitemsContext);
const todoItems = createObj.todoItems;
console.log(todoItems);

return (
  <>
  {
todoItems.map((item)=>(
<AddComponent todoName={item.name} todoDate={item.dueDate} > </AddComponent>

))  }


  </>
);
}
export default Todoitems ;
