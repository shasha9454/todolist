import {AiFillDelete} from 'react-icons/ai';
import {useContext} from "react";
import { TodoitemsContext } from './store/todo-items-store.jsx';


function AddComponent({todoName ,todoDate}){
const {deleteItems} = useContext(TodoitemsContext);
const {todoItems} = useContext(TodoitemsContext);
return (
  <>
  <center>

  <div className="container text-center">
    <div className="row">
      <div className="col">
      {todoName}

      </div>
      <div className="col">
        {todoDate}
      </div>
      <div className="col">
      <button type="button" className="btn btn-danger"

onClick={()=> deleteItems(todoName)}
      ><AiFillDelete/></button>
      </div>

    </div>
  </div>
  </center>
  </>
);
}
export default AddComponent ;
